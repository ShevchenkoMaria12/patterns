import java.util.Scanner;

public class Demo {
    private static ASort aSort;
    static void configure(int id) {
        switch (id) {
            case 1:
                aSort = new FastSort();
            case 2:
                aSort = new BitSort();
            case 3:
                aSort = new PyramidalSort();
        }
    }
    static void render() {
        System.out.println();
        System.out.println("1 - Быстрая сортировка");
        System.out.println("2 - Поразрядная сортировка");
        System.out.println("3 - Пирамидальная сортировка");
        System.out.println("4 - Выход");
    }
    public static void main(String[] args) {
        int id;
        render();
        Scanner scan = new Scanner(System.in);
        id = scan.nextInt();
        do {
            int[] array = {54, 4, 443, 6, 0, 5, 9};
            for (int i = 0; i < array.length; i++) {
                System.out.print(array[i] + " ");
            }
            System.out.println();
            configure(id);
            aSort.ASort(array);
            for (int i = 0; i < array.length; i++) {
                System.out.print(array[i] + " ");
            }
            render();
            id = scan.nextInt();
        }while (id != 4);
    }
}
