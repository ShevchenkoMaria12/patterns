public class BitSort extends ASort{
    @Override
    public Sorting createSorting(int array[]){
        return new BitSorting(array);
    }
}
