public class FastSorting implements Sorting{
    int array[];
    public FastSorting(int array[]){
        this.array = array;
    }
    public int [] sort(){
        quickSort(0, array.length - 1);
        return array;
    }
    public void quickSort(int low, int high) {
        if (array.length == 0)
            return;
        if (low >= high)
            return;
        int middle = low + (high - low) / 2;
        int opor = array[middle];
        int i = low, j = high;
        while (i <= j) {
            while (array[i] < opor) {
                i++;
            }
            while (array[j] > opor) {
                j--;
            }
            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }
        if (low < j)
            quickSort(low, j);
        if (high > i)
            quickSort(i, high);
    }
}
