public abstract class ASort {
    public void ASort(int array[]){
        Sorting sorting = createSorting(array);
        sorting.sort();
    }
    public abstract Sorting createSorting(int array[]);
}
