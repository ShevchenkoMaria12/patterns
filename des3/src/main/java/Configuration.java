import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class Configuration {
    private static Configuration instance;
    private Properties props;
    private Configuration() {
        props = new Properties();
        try {
            FileInputStream fis = new FileInputStream(
                    new File("props.txt"));
            props.load(fis);
        }
        catch (Exception e) {}
    }
    public synchronized static Configuration getInstance() {
        if (instance == null)
            instance = new Configuration();
        return instance;
    }
}
