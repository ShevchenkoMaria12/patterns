public class Client {
    public static void main(String[] args) {
        Animal cat = new HappyAnimal(new Cat());
        Animal dog = new HappyAnimal(new Dog());
        cat.say();
        dog.say();
    }
}
