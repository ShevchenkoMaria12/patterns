public abstract class Animal {
    AnimalImplementor animalImplementor;
    public Animal(AnimalImplementor animalImplementor) {
        this.animalImplementor = animalImplementor;
    }
    public void say() {
        animalImplementor.say();
    }
}
