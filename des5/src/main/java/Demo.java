import java.util.Scanner;

public class Demo {
    private static Application configureApplication() {
        Application app;
        Factory factory;
        Scanner scan = new Scanner(System.in);
        int id = scan.nextInt();
        if (id == 1) {
            factory = new PinkFactory();
            app = new Application(factory);
        } else {
            factory = new BlueFactory();
            app = new Application(factory);
        }
        return app;
    }

    public static void main(String[] args) {
        Application app = configureApplication();
        app.paint();
    }
}
