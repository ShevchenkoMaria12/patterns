public class PinkFactory implements Factory {
    public Dress createDress() {
        return new PinkDress();
    }
    public Shoes createShoes() {
        return new PinkShoes();
    }
}
