public class BlueFactory implements Factory {
    public Dress createDress() {
        return new BlueDress();
    }
    public Shoes createShoes() {
        return new BlueShoes();
    }
}
