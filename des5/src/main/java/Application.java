public class Application {
    private Dress dress;
    private Shoes shoes;

    public Application(Factory factory) {
        dress = factory.createDress();
        shoes = factory.createShoes();
    }

    public void paint() {
        dress.paint();
        shoes.paint();
    }
}
