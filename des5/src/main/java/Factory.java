public interface Factory {
    Dress createDress();
    Shoes createShoes();
}
