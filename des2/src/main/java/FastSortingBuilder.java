public class FastSortingBuilder implements Builder {
    private Sorting sorting;
    public FastSortingBuilder(int array[]){
        sorting = new FastSorting(array);
    }
    public Sorting buildSorting(){
        sorting.sort();
        return sorting;
    }
}
