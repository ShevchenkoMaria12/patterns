import java.util.Scanner;

public class Demo {
    static void render() {
        System.out.println();
        System.out.println("1 - Быстрая сортировка");
        System.out.println("2 - Поразрядная сортировка");
        System.out.println("3 - Пирамидальная сортировка");
        System.out.println("4 - Выход");
    }
    public static void main(String[] args) {
        int id;
        render();
        Scanner scan = new Scanner(System.in);
        id = scan.nextInt();
        do {
            int[] array = {54, 4, 443, 6, 0, 5, 9};
            for (int i = 0; i < array.length; i++) {
                System.out.print(array[i] + " ");
            }
            System.out.println();
            Director director = new Director(id, array);
            director.buildSorting();
            for (int i = 0; i < array.length; i++) {
                System.out.print(array[i] + " ");
            }
            render();
            id = scan.nextInt();
        }while (id != 4);
    }
}
