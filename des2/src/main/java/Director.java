public class Director {
    private Builder builder;
    public Director(int id, int []array) {
        switch (id) {
            case 1:
                builder = new FastSortingBuilder(array);
                break;
            case 2:
                builder = new BitSortingBuilder(array);
                break;
            case 3:
                builder = new PyramidalSortingBuilder(array);
                break;
        }
    }
    public Sorting buildSorting(){
        return builder.buildSorting();
    }
}
