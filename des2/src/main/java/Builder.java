public interface Builder {
    Sorting buildSorting();
}
