public class BitSortingBuilder implements Builder {
    private Sorting sorting;
    public BitSortingBuilder(int array[]){
        sorting = new BitSorting(array);
    }
    public Sorting buildSorting(){
        sorting.sort();
        return sorting;
    }
}
