public class PyramidalSortingBuilder implements Builder {
    private Sorting sorting;
    public PyramidalSortingBuilder(int array[]){
        sorting = new PyramidalSorting(array);
    }
    public Sorting buildSorting(){
        sorting.sort();
        return sorting;
    }
}
