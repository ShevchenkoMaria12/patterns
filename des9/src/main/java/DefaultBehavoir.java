public class DefaultBehavoir implements IBehavoir {
    static public int WAIT = 0;
    static public int ATTACK = 1;
    static public int RUN = 2;
    protected DefaultBehavoir behavoir;
    protected Unit unit;
    public DefaultBehavoir(Unit unit) {
        this.unit = unit;
    }
    public int handle(int distance, int power) {
        if(behavoir != null) {
            return behavoir.handle(distance, power);
        } else {
            System.out.println("DefaultBehavoir::handle()");
            return DefaultBehavoir.WAIT;
        }
    }
    public DefaultBehavoir addBehavoir(DefaultBehavoir behavoir) {
        this.behavoir = behavoir;
        return this;
    }
    static public String getCode(int option) {
        switch(option) {
            case 1:
                return "attack!!!";
            case 2:
                return "run!";
            default:
                return "waiting...";
        }
    }
}
