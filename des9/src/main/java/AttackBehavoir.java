public class AttackBehavoir extends DefaultBehavoir implements IBehavoir {
    public AttackBehavoir(Unit unit) {
        super(unit);
    }
    public int handle(int distance, int power) {
        System.out.println("AttackBehavoir::handle()");
        if((distance < unit.getAttackDistance()) && (power < unit.getPower())) {
            return DefaultBehavoir.ATTACK;
        }
        return super.handle(distance, power);
    }

}
