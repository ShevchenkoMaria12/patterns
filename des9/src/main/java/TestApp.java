public class TestApp {
    public static void main(String[] args) {
        Unit unit = new Unit(5,10);
        unit.setBehavoir(
                new CarefulBehavoir(unit).addBehavoir(
                        new AttackBehavoir(unit).addBehavoir(
                                new DefaultBehavoir(unit)
                        )
                )
        );
        System.out.println(DefaultBehavoir.getCode(unit.processSituation(1,1)));
        System.out.println(DefaultBehavoir.getCode(unit.processSituation(5,10)));
        System.out.println(DefaultBehavoir.getCode(unit.processSituation(20,5)));

        unit.setBehavoir(
                new AngryBehavoir(unit).addBehavoir(
                        new CarefulBehavoir(unit).addBehavoir(
                                new DefaultBehavoir(unit)
                        )
                )
        );
        System.out.println(DefaultBehavoir.getCode(unit.processSituation(1,1)));
        System.out.println(DefaultBehavoir.getCode(unit.processSituation(7,20)));
        System.out.println(DefaultBehavoir.getCode(unit.processSituation(20,5)));
    }
}
