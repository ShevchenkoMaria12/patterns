public class Unit {
    protected int power;
    protected int attackDistance;
    private DefaultBehavoir behavoir;
    public Unit(int power, int attackDistance) {
        this.power = power;
        this.attackDistance = attackDistance;
    }
    public int getPower() {
        return power;
    }
    public int getAttackDistance() {
        return attackDistance;
    }
    protected void setBehavoir(DefaultBehavoir behavoir) {
        this.behavoir = behavoir;
    }
    protected DefaultBehavoir getBehavoir() {
        return behavoir;
    }
    public int processSituation(int distance, int power) {
        return behavoir.handle(distance, power);
    }

}
