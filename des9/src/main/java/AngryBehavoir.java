public class AngryBehavoir extends DefaultBehavoir implements IBehavoir {
    public AngryBehavoir(Unit unit) {
        super(unit);
    }
    public int handle(int distance, int power) {
        System.out.println("AngryBehavoir::handle()");
        if((distance / 2 < unit.getAttackDistance()) && (power / 5 < unit.getPower())) {
            return DefaultBehavoir.ATTACK;
        }
        return super.handle(distance, power);
    }

}
