public class CarefulBehavoir extends DefaultBehavoir implements IBehavoir {
    public CarefulBehavoir(Unit unit) {
        super(unit);
    }
    public int handle(int distance, int power) {
        System.out.println("CarefulBehavoir::handle()");
        if(power < unit.getPower()) {
            if(distance < unit.getAttackDistance()) {
                return DefaultBehavoir.ATTACK;
            } else {
                return DefaultBehavoir.WAIT;
            }
        } else if((distance + 2 < unit.getAttackDistance()) && (power > unit.getPower())) {
            return DefaultBehavoir.RUN;
        }
        return super.handle(distance, power);
    }

}
